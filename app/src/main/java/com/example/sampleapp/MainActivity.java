package com.example.sampleapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.FileProvider;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    AppCompatButton uploadPDF, uploadDOCX;
    Uri uriForPDF, uriForDOCX;
    AppCompatTextView pdfFile, docxFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uploadPDF = findViewById(R.id.upload_pdf);
        uploadDOCX = findViewById(R.id.upload_pdf_and_docx);
        pdfFile = findViewById(R.id.pdf_file_name);
        docxFile = findViewById(R.id.docx_file_name);

        uploadPDF.setOnClickListener(v -> openStorageForPDF());

        uploadDOCX.setOnClickListener(v -> openStorageForDOCX());

        pdfFile.setOnClickListener(v -> openFile(uriForPDF, "application/pdf"));

        docxFile.setOnClickListener(v -> openFile(uriForDOCX, "application/msword"));

    }

    private void openFile(Uri uri, String mimeType) {
        File file = new File(getExternalFilesDir(null).getPath() + "/" + new File(uri.getPath()).getName());
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(FileProvider.getUriForFile(MainActivity.this, "com.example.sampleapp.provider", file), mimeType);
        target.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        target.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "There is no app to open this file. Please download PDF reader from play store", Toast.LENGTH_SHORT).show();
        }
    }

    private void openStorageForPDF() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);
    }

    private void openStorageForDOCX() {
        String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"};
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimetypes);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Doc/Docx"), 2);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case 1 :
                    if (data != null) {
                        try {
                            Uri uri = data.getData();
                            Cursor returnCursor =
                                    getContentResolver().query(uri, null, null, null, null);
                            /*
                             * Get the column indexes of the data in the Cursor,
                             * move to the first row in the Cursor, get the data,
                             * and display it.
                             */
                            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                            returnCursor.moveToFirst();
                            pdfFile.setText(returnCursor.getString(nameIndex));
                            pdfFile.setVisibility(View.VISIBLE);
                            readTextFromUri(uri);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(this, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case 2 :
                    if (data != null) {
                        uriForDOCX = Uri.parse(data.getData().getPath());
                        try {
                            File file = new File(getRealPathFromURI(uriForDOCX));
                            docxFile.setText(file.getName());
                            docxFile.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(this, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    private String readTextFromUri(Uri uri) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream =
                     getContentResolver().openInputStream(uri);
             BufferedReader reader = new BufferedReader(
                     new InputStreamReader(Objects.requireNonNull(inputStream)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                Log.d("TESTING", line);
            }
        }
        return stringBuilder.toString();
    }
}